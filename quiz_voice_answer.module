<?php

/**
 * @file
 * The main file for quiz_voice_answer.module.
 *
 * Directions are implemented as a question type. However, they do not affect
 * the score.
 */

/**
 * Implements hook_help().
 */
function quiz_voice_answer_help($path, $arg) {
  switch ($path) {
    case 'admin/help#quiz_voice_answer':
      $output = '<h3>' . t('Overview') . '</h3>';
      $output .= '<p>' . t('Quiz voice answer module provides a new content type to use with quiz module as a question type. It allows users to record and save voice message as a response to quiz question.') . '</p>';
      $output .= '<h3>' . t('Requirements') . '</h3>';
      $output .= '<ul>';
      $output .= '<li>' . t('<a href="@href">Quiz</a>', array('@href' => url('https://www.drupal.org/project/quiz'))) . '</li>';
      $output .= '<li>' . t('<a href="@href">Libraries API</a>', array('@href' => url('https://www.drupal.org/project/libraries'))) . '</li>';
      $output .= '<li>' . t('<a href="@href">File entity (fieldable files)</a>', array('@href' => url('https://www.drupal.org/project/file_entity'))) . '</li>';
      $output .= '</ul>';
      $output .= '<h3>' . t('Installation') . '</h3>';
      $output .= '<ol>';
      $output .= '<li>' . t('Copy record question folder to modules directory (usually <strong>sites/all/modules</strong>);') . '</li>';
      $output .= '<li>' . t('Download the library <a href="@href">Recordmp3js-Drupal</a>;', array('@href' => url('https://github.com/Ruslan03492/Recordmp3js-Drupal'))) . '</li>';
      $output .= '<li>' . t('Insert the contents of the archive here <strong>sites/all/libraries/Recordmp3js-Drupal</strong>;') . '</li>';
      $output .= '<li>' . t('At <strong>admin/build/modules</strong> enable the Quiz voice answer module in the Quiz Question package;') . '</li>';
      $output .= '<li>' . t('Hit "Save configuration" button.') . '</li>';
      $output .= '</ol>';
      $output .= '<p>' . t('For detailed instructions on installing contributed modules see: <a href="@href">Installing modules (Drupal 7)</a>.', array('@href' => url('http://drupal.org/documentation/install/modules-themes/modules-7'))) . '</p>';
      return $output;
  }
}

/**
 * Implements hook_library().
 *
 * Return information about external libraries.
 */
function quiz_voice_answer_libraries_info() {
  $libraries['Recordmp3js-Drupal'] = array(
    'name' => 'Recordmp3js Drupal',
    'vendor url' => 'https://github.com/Ruslan03492/Recordmp3js-Drupal',
    'download url' => 'https://github.com/Ruslan03492/Recordmp3js-Drupal.git',
    'version' => '1.0.0',
    'files' => array(
      'js' => array(
        'recordmp3.js' => array(
          'scope' => 'footer',
          'weight' => 5,
        ),
        'quiz_voice_answer.js' => array(
          'scope' => 'footer',
          'weight' => 5,
        ),
      ),
    ),
  );
  return $libraries;
}

/**
 * Implements hook_quiz_question_info().
 */
function quiz_voice_answer_quiz_question_info() {
  return array(
    'quiz_voice_answer' => array(
      'name' => t('Quiz voice answer'),
      'description' => t(
        'Quiz directions allow you to drop in directions
      anywhere in a quiz. You can also use them to insert a confirmation
      step at the end of the quiz.'
      ),
      'question provider' => 'QuizVoiceAnswerQuestion',
      'response provider' => 'QuizVoiceAnswerResponse',
      'module' => 'quiz_question',
      // All wrapper functions are in that module.
    ),
  );
}

/**
 * Implements hook_config().
 *
 * Ban editing field quiz_voice_answer.
 */
function quiz_voice_answer_config() {
  return FALSE;
  // No config options available for this question type.
}

/**
 * Submit function for the report form.
 *
 * @param array $values
 *   The FAPI $form_state['values'].
 */
function quiz_voice_answer_report_submit(array $values) {
  quiz_voice_answer_score_an_answer($values);
}

/**
 * Validation function for the report form.
 *
 * @param array $values
 *   The FAPI $form_state['values'].
 * @param string $form_key
 *   Array key to add errors to.
 */
function quiz_voice_answer_report_validate(array $values, $form_key) {
  $max = (int) $values['max_score'];
  // Check to make sure that entered score is not higher than max allowed score.
  if (!_quiz_is_int($values['score'], 0, $max)) {
    form_set_error($form_key . '][score', t('The score needs to be a number between @min and @max', array(
      '@min' => 0,
      '@max' => $max,
    )));
  }
}

/**
 * Set a score for a long answer question.
 *
 * This stores a score for a long answer question and marks that question as
 * having been evaluated.The function updates all of the necessary data sources
 * so that the individual answer results should be reflected in the total
 * scoring table.
 *
 * @return int
 *   Number of scores adjusted. If a change was made, this should be 1.
 */
function quiz_voice_answer_score_an_answer($values) {
  $nid = $values['nid'];
  $vid = $values['vid'];
  $score = $values['score'];
  $rid = $values['rid'];
  $answer_feedback = $values['answer_feedback'];
  $quiz = $values['quiz'];
  // We update all three.
  // First, we update the long answer table.
  $question_max_score = db_select('quiz_question_properties', 'qqp')
    ->fields('qqp', array('max_score'))
    ->condition('qqp.nid', $nid)
    ->condition('qqp.vid', $vid)
    ->execute()
    ->fetchField();

  $quiz_max_score = db_select('quiz_node_relationship', 'qnr')
    ->fields('qnr', array('max_score'))
    ->condition('qnr.parent_vid', $quiz->vid)
    ->condition('qnr.child_vid', $vid)
    ->execute()
    ->fetchField();

  $changed = db_update('quiz_voice_answer_user_answers')
    ->fields(
      array(
        'score' => $score * $question_max_score / $quiz_max_score,
        'is_evaluated' => 1,
        'answer_feedback' => $answer_feedback,
      )
    )
    ->condition('question_nid', $nid)
    ->condition('question_vid', $vid)
    ->condition('result_id', $rid)
    ->execute();
  return $changed;
}
